package sample;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.Window;

import java.io.*;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Controller {
    @FXML
    TextArea editorText;
    int tamanyActual = 15;

    //Sortir de l'editor de text
    public void sortir(ActionEvent actionEvent) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Error");
        alert.setHeaderText(null);
        alert.setContentText("No hi ha cap text a guardar");

        alert.showAndWait();

        Platform.exit();
    }

    //Cambiar les fonts
    public void font1(ActionEvent actionEvent) {
        editorText.setFont(Font.font("Cantarell", FontWeight.NORMAL, tamanyActual));
    }

    public void font2(ActionEvent actionEvent) {
        editorText.setFont(Font.font("DejaVu Sans Mono", FontWeight.NORMAL, tamanyActual));
    }

    public void font3(ActionEvent actionEvent) {
        editorText.setFont(Font.font("WenQuanYi Zen Hei", FontWeight.NORMAL, tamanyActual));
    }

    //Cambiar el tamany de la lletra
    public void petit(ActionEvent actionEvent) {
        editorText.setStyle("-fx-font-size: 15;");
        tamanyActual = 15;
    }

    public void mitja(ActionEvent actionEvent) {
        editorText.setStyle("-fx-font-size: 20;");
        tamanyActual = 20;
    }

    public void gran(ActionEvent actionEvent) {
        editorText.setStyle("-fx-font-size: 25;");
        tamanyActual = 25;
    }

    //Informació sobre l'editor de text
    public void about(ActionEvent actionEvent) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Sobre l'Editor");
        alert.setHeaderText(null);
        alert.setContentText("Aquesta es la primera versió de l'editor!");

        alert.showAndWait();
    }

    //Guardar un fitxer
    public void guardar(ActionEvent actionEvent) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Obrir un fitxer");
        fileChooser.getExtensionFilters().addAll();

        Window mainStage = editorText.getScene().getWindow();

        File selectedFile = fileChooser.showSaveDialog(mainStage);
        if (editorText.getText().length() != 0) {
            try {
                FileWriter fileWriter = null;

                fileWriter = new FileWriter(selectedFile);
                fileWriter.write(editorText.getText());
                fileWriter.close();
            } catch (IOException ex) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Error");
            alert.setHeaderText(null);
            alert.setContentText("No hi ha cap text a guardar");

            alert.showAndWait();
        }
    }

    //Obrir un fitxer
    public void obrir(ActionEvent actionEvent) throws FileNotFoundException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Obrir un fitxer");
        fileChooser.getExtensionFilters().addAll();

        Window mainStage = editorText.getScene().getWindow();

        File selectedFile = fileChooser.showOpenDialog(mainStage);

        if (selectedFile != null) {
            StringBuilder stringBuffer = new StringBuilder();
            BufferedReader bufferedReader = null;

            try {
                bufferedReader = new BufferedReader(new FileReader(selectedFile));

                String text;
                while ((text = bufferedReader.readLine()) != null) {
                    stringBuffer.append(text);
                }

            } catch (FileNotFoundException ex) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    bufferedReader.close();
                } catch (IOException ex) {
                    Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            editorText.setText(stringBuffer.toString());
            Stage stg = (Stage) mainStage;
            stg.setTitle(selectedFile.getName());
        }
    }
}
